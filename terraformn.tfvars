region = "us-east-1"
cidr_block = "172.36.0.0/16"
project = "mahi"
cidr_pub_subnet = "172.36.1.0/24"
pub_availability_zone = "us-east-1a"
cidr_pvt_subnet = "172.36.2.0/24"
pvt_availability_zone = "us-east-1b"