variable "region" {
  type = string
}
variable "cidr_block" {
  type = string
}
variable "project" {
  type = string
}
variable "cidr_pub_subnet" {
  type = string
}
variable "pub_availability_zone" {
  type = string
}
variable "cidr_pvt_subnet" {
  type = string
}
variable "pvt_availability_zone" {
  type = string
}