
provider "aws" {
  region = var.region
}

#create s3 backend
terraform {
  backend "s3" {
    bucket = "demobucket132"
    key    = "mahi/file"
    region =  "us-east-1"
  }
}
#create aws vpc
resource "aws_vpc" "vpc" {
  cidr_block       =  var.cidr_block
  instance_tenancy = "default"

  tags = {
    Name = var.project
  }
}
#create internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = var.project
  }
}
#create aws subnet
resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.cidr_pub_subnet
  availability_zone = var.pub_availability_zone

  tags = {
    Name = var.project
  }
}
#create aws private subnet
resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.vpc.id
  cidr_block = var.cidr_pvt_subnet
  availability_zone = var.pvt_availability_zone

  tags = {
    Name = var.project
  }
}
resource "aws_route_table" "pub" {
  vpc_id = aws_vpc.vpc.id

   tags = {
    Name = var.project
  }
}
resource "aws_route_table" "pvt" {
  vpc_id = aws_vpc.vpc.id

 
  tags = {
    Name = var.project
  }
}
resource "aws_route" "rpub" {
  route_table_id            = "aws_route_table.pub.id"
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id = "aws_internet_gateway.gw.id"
  #depends_on                = [aws_route_table.pub]
}


#resource "aws_route" "rpvt" {
 # route_table_id            = "aws_route_table.pvt.id"
  #destination_cidr_block    = "var.cidr_pvt_subnet"
   
#}

resource "aws_route_table_association" "pub" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.pub.id
}
resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private.id
  route_table_id = aws_route_table.pvt.id
}

